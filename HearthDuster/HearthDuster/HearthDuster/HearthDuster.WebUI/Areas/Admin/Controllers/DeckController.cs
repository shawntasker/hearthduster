﻿using HearthDuster.DataAccess;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using HearthDuster.WebUI.Areas.Admin.Models;

namespace HearthDuster.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class DeckController : Controller
    {
        private hearthdusterdbEntities _db = new hearthdusterdbEntities();
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public DeckController()
        {

        }

        public DeckController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        // GET: Admin/AdminDeck
        public ActionResult Index()
        {
            var decks = _db.Decks.ToList();
            return View(decks);
        }

        
    }
}