﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HearthDuster.DataAccess;

namespace HearthDuster.WebUI.Areas.Admin.Controllers
{
    public class ImageController : Controller
    {
        private hearthdusterdbEntities _db = new hearthdusterdbEntities();

        // GET: Admin/Image
        public ActionResult Index(int id)
        {
            var imageToRetrieve = _db.Images.Find(id);
            return File(imageToRetrieve.Content, imageToRetrieve.ContentType);
        }
    }
}