﻿using HearthDuster.DataAccess;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HearthDuster.Domain;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Data.Entity;

namespace HearthDuster.WebUI.Areas.Admin.Controllers
{
    //[Authorize(Roles = "Administrator")]
    public class CardController : Controller
    {
        private hearthdusterdbEntities _db = new hearthdusterdbEntities();

        // GET: Admin
        public ActionResult Index()
        {
            var cards = _db.Cards.ToList();
            return View(cards);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Domain.Card card = _db.Cards.Include(c => c.Image).SingleOrDefault(c => c.CardId == id);
            if(card == null)
            {
                return HttpNotFound();
            }

            return View(card);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HearthDuster.Domain.Card card, HttpPostedFile upload)
        {
            try
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    var cardImage = new Image
                    {
                        original_file_name = Path.GetFileName(upload.FileName),
                        mime_type = "Card Image",
                        ContentType = upload.ContentType
                    };
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        cardImage.Content = reader.ReadBytes(upload.ContentLength);
                    }
                    card.Image = new Image();
                }
            }
            catch(RetryLimitExceededException dex)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            return View(card);

            


            //// old try
            //if(ModelState.IsValid)
            //{
            //    if(picture != null)
            //    {
            //        //card.picture_mime_type = picture.ContentType;
            //        //card.picture = new byte[picture.ContentLength];
            //        //picture.InputStream.Read(card.picture, 0, picture.ContentLength);
            //    }
            //    _db.Cards.Add(card);
            //    TempData["message"] = string.Format("{0}, has been saved", card.Name);
            //    _db.SaveChanges();
            //    return RedirectToAction("Index");
            //}else
            //{
            //    // there is something wrong with the data values
            //    return View(card);
            //}
        }

        public static byte[] ImageToBinary(string imagePath)
        {
            FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[fileStream.Length];
            fileStream.Read(buffer, 0, (int)fileStream.Length);
            fileStream.Close();
            return buffer;
        }
    }
}