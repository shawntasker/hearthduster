﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HearthDuster.Domain;
using System.ComponentModel.DataAnnotations;
using HearthDuster.Domain.Interfaces;

namespace HearthDuster.WebUI.Areas.Admin.Models
{
    public class CreateCardViewModel : ICard
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public int Health { get; set; }

        public int Mana { get; set; }

        public int Attack { get; set; }

        public string Rarity { get; set; }

        public string CardSet { get; set; }

        public int CreateValue { get; set; }

        public int DestroyValue { get; set; }

        public int MechanicCardId { get; set; }

        public int ImageId { get; set; }

        public Card Card { get; set; }
    }
}