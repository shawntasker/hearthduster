﻿using HearthDuster.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HearthDuster.WebUI.Models
{
    public class CreateCardViewModel : ICard
    {
        public int Id { get; set; }
    }
}