﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HearthDuster.WebUI.Models
{
    public class CardViewModel
    {
        private readonly List<Card> cardtypes;

        [Display(Name = "Card Type")]
        public int SelectedTypeId { get; set; }

        public IEnumerable<SelectListItem> TypeItems
        {
            get { return new SelectList(cardtypes, "Id", "Name"); }
        }
    }
}