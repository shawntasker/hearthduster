﻿using HearthDuster.DataAccess;
using HearthDuster.WebUI.Areas.Admin.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HearthDuster.WebUI.Controllers
{
    public class DeckController : Controller
    {
        // GET: Deck
        public ActionResult Index()
        {
            return View();
        }

        private hearthdusterdbEntities _db = new hearthdusterdbEntities();
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public DeckController()
        {

        }

        public DeckController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }


        [HttpPost]
        public ActionResult Create(CreateCardViewModel model)
        {
            return View();

        }

        [HttpGet]
        public ActionResult Create()
        {
            var user = _db.AspNetUsers.Find(User.Identity.Equals(User.Identity.GetUserId()));
            var cards = user.Cards;
            return View(cards);
        }
    }
}