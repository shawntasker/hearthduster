﻿using HearthDuster.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HearthDuster.WebUI.Controllers
{
    public class CardController : Controller
    {
        private hearthdusterdbEntities _db = new hearthdusterdbEntities();
        // GET: Card
        public ActionResult Index()
        {
            var cards = _db.Cards.ToList();

            return View(cards);
        }

        //public ActionResult Collection(int id)
        //{
        //    var collection = from cardId in _db.Cards
        //                      where cardId.AspNetUsers.Equals(id)
        //                      select cardId;

        //    var collectionList = collection.ToList();

        //    return View(collectionList);
        //}
    }
}