﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HearthDuster.WebUI.Startup))]
namespace HearthDuster.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
