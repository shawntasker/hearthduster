﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HearthDuster.Domain.Enums
{
    public enum Rarity
    {
        [Description("Common")]
        Common,
        [Description("Rare")]
        Rare,
        [Description("Epic")]
        Epic,
        [Description("Legendary")]
        Legendary
    }
}
