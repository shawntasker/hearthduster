﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HearthDuster.Domain.Enums
{
    public enum CardType
    {
        [Description("Minion")]
        Minion,
        [Description("Spell")]
        Spell,
        [Description("Weapon")]
        Weapon
    }
}
