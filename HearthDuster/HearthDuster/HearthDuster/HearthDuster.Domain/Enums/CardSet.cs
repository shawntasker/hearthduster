﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HearthDuster.Domain.Enums
{
    public enum CardSet
    {
        [Description("Basic")]
        Basic,
        [Description("Classic")]
        Classic,
        [Description("Reward")]
        Reward,
        [Description("Naxxramas")]
        Naxxramas,
        [Description("Goblins vs Gnomes")]
        GoblinsVsGnomes,
        [Description("Blackrock Mountain")]
        BlackRockMountain
    }
}
