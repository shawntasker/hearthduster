﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HearthDuster.Domain.Enums
{
    public enum DeckClass
    {
        [Description("Druid")]
        Druid,
        [Description("Hunter")]
        Hunter,
        [Description("Mage")]
        Mage,
        [Description("Paladin")]
        Paladin,
        [Description("Priest")]
        Priest,
        [Description("Rogue")]
        Rogue,
        [Description("Shaman")]
        Shaman,
        [Description("Warlock")]
        Warlock,
        [Description("Warrior")]
        Warrior,
    }
}
