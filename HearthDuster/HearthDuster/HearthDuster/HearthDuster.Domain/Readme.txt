﻿The Domain Layer is being generated from the EDMX file found in the DataAccess Class Library. 
This happens because the following line points to the EDMX from the DataAccess Layer.

const string inputFile = @"../HearthDuster.DataAccess/HearthDusterDataModel.edmx";

Steps to update the Entity Classes:

1.) Right-click on the .tt file and select "Run Custom Tool"