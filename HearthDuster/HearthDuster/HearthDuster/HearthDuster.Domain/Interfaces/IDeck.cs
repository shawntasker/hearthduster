﻿using System.Collections.Generic;

namespace HearthDuster.Domain
{
    public interface IDeck
    {
        string author { get; set; }
        ICollection<Card> Cards { get; set; }
        ICollection<Comment> Comments { get; set; }
        int? cost { get; set; }
        string description { get; set; }
        int? dislikes { get; set; }
        int id { get; set; }
        int? likes { get; set; }
        string name { get; set; }
        int user_id { get; set; }
    }
}