﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HearthDuster.Domain.Interfaces
{
    public interface ICard 
    {
        int Id { get; set; }
    }
}
