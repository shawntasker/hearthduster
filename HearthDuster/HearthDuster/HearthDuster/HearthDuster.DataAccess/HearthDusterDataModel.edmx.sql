
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 10/11/2015 02:39:21
-- Generated from EDMX file: C:\Users\Chapes\Source\Workspaces\HearthDuster\HearthDuster\HearthDuster.DataAccess\HearthDusterDataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [hearthdusterdb];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_CardDecks_Cards]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CardDecks] DROP CONSTRAINT [FK_CardDecks_Cards];
GO
IF OBJECT_ID(N'[dbo].[FK_CardDecks_Decks]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CardDecks] DROP CONSTRAINT [FK_CardDecks_Decks];
GO
IF OBJECT_ID(N'[dbo].[FK_CardMechanics_Cards]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CardMechanics] DROP CONSTRAINT [FK_CardMechanics_Cards];
GO
IF OBJECT_ID(N'[dbo].[FK_CardMechanics_Mechanics]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CardMechanics] DROP CONSTRAINT [FK_CardMechanics_Mechanics];
GO
IF OBJECT_ID(N'[dbo].[FK_Cards_Files]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Cards] DROP CONSTRAINT [FK_Cards_Files];
GO
IF OBJECT_ID(N'[dbo].[FK_Comments_AspNetUsers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Comments] DROP CONSTRAINT [FK_Comments_AspNetUsers];
GO
IF OBJECT_ID(N'[dbo].[FK_Comments_Cards]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Comments] DROP CONSTRAINT [FK_Comments_Cards];
GO
IF OBJECT_ID(N'[dbo].[FK_Comments_Decks]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Comments] DROP CONSTRAINT [FK_Comments_Decks];
GO
IF OBJECT_ID(N'[dbo].[FK_Comments_Mechanics]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Comments] DROP CONSTRAINT [FK_Comments_Mechanics];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_AspNetUserRoles_dbo_AspNetRoles_RoleId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo_AspNetUserRoles_dbo_AspNetRoles_RoleId];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_AspNetUserRoles_dbo_AspNetUsers_UserId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo_AspNetUserRoles_dbo_AspNetUsers_UserId];
GO
IF OBJECT_ID(N'[dbo].[FK_Files_AspNetUsers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Files] DROP CONSTRAINT [FK_Files_AspNetUsers];
GO
IF OBJECT_ID(N'[dbo].[FK_UserCards_AspNetUsers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserCards] DROP CONSTRAINT [FK_UserCards_AspNetUsers];
GO
IF OBJECT_ID(N'[dbo].[FK_UserCards_Cards]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserCards] DROP CONSTRAINT [FK_UserCards_Cards];
GO
IF OBJECT_ID(N'[dbo].[FK_UserDecks_Cards]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserDecks] DROP CONSTRAINT [FK_UserDecks_Cards];
GO
IF OBJECT_ID(N'[dbo].[FK_UserDecks_Decks]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserDecks] DROP CONSTRAINT [FK_UserDecks_Decks];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[__MigrationHistory]', 'U') IS NOT NULL
    DROP TABLE [dbo].[__MigrationHistory];
GO
IF OBJECT_ID(N'[dbo].[AspNetRoles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetRoles];
GO
IF OBJECT_ID(N'[dbo].[AspNetUserClaims]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUserClaims];
GO
IF OBJECT_ID(N'[dbo].[AspNetUserLogins]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUserLogins];
GO
IF OBJECT_ID(N'[dbo].[AspNetUserRoles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUserRoles];
GO
IF OBJECT_ID(N'[dbo].[AspNetUsers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUsers];
GO
IF OBJECT_ID(N'[dbo].[CardDecks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CardDecks];
GO
IF OBJECT_ID(N'[dbo].[CardMechanics]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CardMechanics];
GO
IF OBJECT_ID(N'[dbo].[Cards]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Cards];
GO
IF OBJECT_ID(N'[dbo].[Comments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Comments];
GO
IF OBJECT_ID(N'[dbo].[Decks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Decks];
GO
IF OBJECT_ID(N'[dbo].[Files]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Files];
GO
IF OBJECT_ID(N'[dbo].[Mechanics]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Mechanics];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[UserCards]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserCards];
GO
IF OBJECT_ID(N'[dbo].[UserDecks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserDecks];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'C__MigrationHistory'
CREATE TABLE [dbo].[C__MigrationHistory] (
    [MigrationId] nvarchar(150)  NOT NULL,
    [ContextKey] nvarchar(300)  NOT NULL,
    [Model] varbinary(max)  NOT NULL,
    [ProductVersion] nvarchar(32)  NOT NULL
);
GO

-- Creating table 'AspNetRoles'
CREATE TABLE [dbo].[AspNetRoles] (
    [Id] nvarchar(128)  NOT NULL,
    [Name] nvarchar(256)  NOT NULL
);
GO

-- Creating table 'AspNetUserClaims'
CREATE TABLE [dbo].[AspNetUserClaims] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserId] nvarchar(128)  NOT NULL,
    [ClaimType] nvarchar(max)  NULL,
    [ClaimValue] nvarchar(max)  NULL
);
GO

-- Creating table 'AspNetUserLogins'
CREATE TABLE [dbo].[AspNetUserLogins] (
    [LoginProvider] nvarchar(128)  NOT NULL,
    [ProviderKey] nvarchar(128)  NOT NULL,
    [UserId] nvarchar(128)  NOT NULL
);
GO

-- Creating table 'AspNetUsers'
CREATE TABLE [dbo].[AspNetUsers] (
    [Id] nvarchar(128)  NOT NULL,
    [Email] nvarchar(256)  NULL,
    [EmailConfirmed] bit  NOT NULL,
    [PasswordHash] nvarchar(max)  NULL,
    [SecurityStamp] nvarchar(max)  NULL,
    [PhoneNumber] nvarchar(max)  NULL,
    [PhoneNumberConfirmed] bit  NOT NULL,
    [TwoFactorEnabled] bit  NOT NULL,
    [LockoutEndDateUtc] datetime  NULL,
    [LockoutEnabled] bit  NOT NULL,
    [AccessFailedCount] int  NOT NULL,
    [UserName] nvarchar(256)  NOT NULL
);
GO

-- Creating table 'Cards'
CREATE TABLE [dbo].[Cards] (
    [CardId] int IDENTITY(1,1) NOT NULL,
    [FileId] int  NULL,
    [Name] varchar(50)  NOT NULL,
    [Health] int  NULL,
    [Mana] int  NULL,
    [Attack] int  NULL,
    [Rarity] varchar(50)  NULL,
    [CardSet] varchar(50)  NOT NULL,
    [Type] varchar(50)  NOT NULL,
    [Text] varchar(50)  NULL,
    [CreateValue] int  NULL,
    [DestroyValue] int  NULL,
    [Durability] int  NULL
);
GO

-- Creating table 'Comments'
CREATE TABLE [dbo].[Comments] (
    [CommentId] int IDENTITY(1,1) NOT NULL,
    [CardId] int  NULL,
    [MechanicsId] int  NULL,
    [DeckId] int  NULL,
    [UserId] nvarchar(128)  NOT NULL,
    [LastEditedTime] datetime  NULL,
    [CreatedTime] datetime  NOT NULL,
    [Text] varchar(max)  NOT NULL,
    [CurrentPatch] varchar(45)  NULL,
    [Likes] int  NOT NULL,
    [Dislikes] int  NOT NULL
);
GO

-- Creating table 'Decks'
CREATE TABLE [dbo].[Decks] (
    [DeckId] int IDENTITY(1,1) NOT NULL,
    [UserId] nvarchar(128)  NOT NULL,
    [Class] varchar(50)  NULL,
    [Name] varchar(50)  NULL,
    [Cost] int  NULL,
    [Description] varchar(max)  NULL,
    [Author] varchar(50)  NULL,
    [Likes] int  NULL,
    [Dislikes] int  NULL
);
GO

-- Creating table 'Files'
CREATE TABLE [dbo].[Files] (
    [FileId] int IDENTITY(1,1) NOT NULL,
    [OriginalFileName] nvarchar(max)  NOT NULL,
    [FileData] varbinary(max)  NOT NULL,
    [MimeType] nvarchar(max)  NOT NULL,
    [DateUploaded] datetime  NOT NULL,
    [UserId] nvarchar(128)  NOT NULL
);
GO

-- Creating table 'Mechanics'
CREATE TABLE [dbo].[Mechanics] (
    [MechanicId] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(45)  NOT NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'AspNetUserRoles'
CREATE TABLE [dbo].[AspNetUserRoles] (
    [AspNetRoles_Id] nvarchar(128)  NOT NULL,
    [AspNetUsers_Id] nvarchar(128)  NOT NULL
);
GO

-- Creating table 'CardDecks'
CREATE TABLE [dbo].[CardDecks] (
    [Cards_CardId] int  NOT NULL,
    [Decks_DeckId] int  NOT NULL
);
GO

-- Creating table 'CardMechanics'
CREATE TABLE [dbo].[CardMechanics] (
    [Cards_CardId] int  NOT NULL,
    [Mechanics_MechanicId] int  NOT NULL
);
GO

-- Creating table 'UserCards'
CREATE TABLE [dbo].[UserCards] (
    [AspNetUsers_Id] nvarchar(128)  NOT NULL,
    [Cards_CardId] int  NOT NULL
);
GO

-- Creating table 'UserDecks'
CREATE TABLE [dbo].[UserDecks] (
    [Cards1_CardId] int  NOT NULL,
    [Decks1_DeckId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [MigrationId], [ContextKey] in table 'C__MigrationHistory'
ALTER TABLE [dbo].[C__MigrationHistory]
ADD CONSTRAINT [PK_C__MigrationHistory]
    PRIMARY KEY CLUSTERED ([MigrationId], [ContextKey] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetRoles'
ALTER TABLE [dbo].[AspNetRoles]
ADD CONSTRAINT [PK_AspNetRoles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetUserClaims'
ALTER TABLE [dbo].[AspNetUserClaims]
ADD CONSTRAINT [PK_AspNetUserClaims]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [LoginProvider], [ProviderKey], [UserId] in table 'AspNetUserLogins'
ALTER TABLE [dbo].[AspNetUserLogins]
ADD CONSTRAINT [PK_AspNetUserLogins]
    PRIMARY KEY CLUSTERED ([LoginProvider], [ProviderKey], [UserId] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetUsers'
ALTER TABLE [dbo].[AspNetUsers]
ADD CONSTRAINT [PK_AspNetUsers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [CardId] in table 'Cards'
ALTER TABLE [dbo].[Cards]
ADD CONSTRAINT [PK_Cards]
    PRIMARY KEY CLUSTERED ([CardId] ASC);
GO

-- Creating primary key on [CommentId] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [PK_Comments]
    PRIMARY KEY CLUSTERED ([CommentId] ASC);
GO

-- Creating primary key on [DeckId] in table 'Decks'
ALTER TABLE [dbo].[Decks]
ADD CONSTRAINT [PK_Decks]
    PRIMARY KEY CLUSTERED ([DeckId] ASC);
GO

-- Creating primary key on [FileId] in table 'Files'
ALTER TABLE [dbo].[Files]
ADD CONSTRAINT [PK_Files]
    PRIMARY KEY CLUSTERED ([FileId] ASC);
GO

-- Creating primary key on [MechanicId] in table 'Mechanics'
ALTER TABLE [dbo].[Mechanics]
ADD CONSTRAINT [PK_Mechanics]
    PRIMARY KEY CLUSTERED ([MechanicId] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [AspNetRoles_Id], [AspNetUsers_Id] in table 'AspNetUserRoles'
ALTER TABLE [dbo].[AspNetUserRoles]
ADD CONSTRAINT [PK_AspNetUserRoles]
    PRIMARY KEY CLUSTERED ([AspNetRoles_Id], [AspNetUsers_Id] ASC);
GO

-- Creating primary key on [Cards_CardId], [Decks_DeckId] in table 'CardDecks'
ALTER TABLE [dbo].[CardDecks]
ADD CONSTRAINT [PK_CardDecks]
    PRIMARY KEY CLUSTERED ([Cards_CardId], [Decks_DeckId] ASC);
GO

-- Creating primary key on [Cards_CardId], [Mechanics_MechanicId] in table 'CardMechanics'
ALTER TABLE [dbo].[CardMechanics]
ADD CONSTRAINT [PK_CardMechanics]
    PRIMARY KEY CLUSTERED ([Cards_CardId], [Mechanics_MechanicId] ASC);
GO

-- Creating primary key on [AspNetUsers_Id], [Cards_CardId] in table 'UserCards'
ALTER TABLE [dbo].[UserCards]
ADD CONSTRAINT [PK_UserCards]
    PRIMARY KEY CLUSTERED ([AspNetUsers_Id], [Cards_CardId] ASC);
GO

-- Creating primary key on [Cards1_CardId], [Decks1_DeckId] in table 'UserDecks'
ALTER TABLE [dbo].[UserDecks]
ADD CONSTRAINT [PK_UserDecks]
    PRIMARY KEY CLUSTERED ([Cards1_CardId], [Decks1_DeckId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UserId] in table 'AspNetUserClaims'
ALTER TABLE [dbo].[AspNetUserClaims]
ADD CONSTRAINT [FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId'
CREATE INDEX [IX_FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId]
ON [dbo].[AspNetUserClaims]
    ([UserId]);
GO

-- Creating foreign key on [UserId] in table 'AspNetUserLogins'
ALTER TABLE [dbo].[AspNetUserLogins]
ADD CONSTRAINT [FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId'
CREATE INDEX [IX_FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId]
ON [dbo].[AspNetUserLogins]
    ([UserId]);
GO

-- Creating foreign key on [UserId] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [FK_Comments_AspNetUsers]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Comments_AspNetUsers'
CREATE INDEX [IX_FK_Comments_AspNetUsers]
ON [dbo].[Comments]
    ([UserId]);
GO

-- Creating foreign key on [UserId] in table 'Files'
ALTER TABLE [dbo].[Files]
ADD CONSTRAINT [FK_Files_AspNetUsers]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Files_AspNetUsers'
CREATE INDEX [IX_FK_Files_AspNetUsers]
ON [dbo].[Files]
    ([UserId]);
GO

-- Creating foreign key on [FileId] in table 'Cards'
ALTER TABLE [dbo].[Cards]
ADD CONSTRAINT [FK_Cards_Files]
    FOREIGN KEY ([FileId])
    REFERENCES [dbo].[Files]
        ([FileId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Cards_Files'
CREATE INDEX [IX_FK_Cards_Files]
ON [dbo].[Cards]
    ([FileId]);
GO

-- Creating foreign key on [CardId] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [FK_Comments_Cards]
    FOREIGN KEY ([CardId])
    REFERENCES [dbo].[Cards]
        ([CardId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Comments_Cards'
CREATE INDEX [IX_FK_Comments_Cards]
ON [dbo].[Comments]
    ([CardId]);
GO

-- Creating foreign key on [DeckId] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [FK_Comments_Decks]
    FOREIGN KEY ([DeckId])
    REFERENCES [dbo].[Decks]
        ([DeckId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Comments_Decks'
CREATE INDEX [IX_FK_Comments_Decks]
ON [dbo].[Comments]
    ([DeckId]);
GO

-- Creating foreign key on [MechanicsId] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [FK_Comments_Mechanics]
    FOREIGN KEY ([MechanicsId])
    REFERENCES [dbo].[Mechanics]
        ([MechanicId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Comments_Mechanics'
CREATE INDEX [IX_FK_Comments_Mechanics]
ON [dbo].[Comments]
    ([MechanicsId]);
GO

-- Creating foreign key on [AspNetRoles_Id] in table 'AspNetUserRoles'
ALTER TABLE [dbo].[AspNetUserRoles]
ADD CONSTRAINT [FK_AspNetUserRoles_AspNetRoles]
    FOREIGN KEY ([AspNetRoles_Id])
    REFERENCES [dbo].[AspNetRoles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [AspNetUsers_Id] in table 'AspNetUserRoles'
ALTER TABLE [dbo].[AspNetUserRoles]
ADD CONSTRAINT [FK_AspNetUserRoles_AspNetUsers]
    FOREIGN KEY ([AspNetUsers_Id])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AspNetUserRoles_AspNetUsers'
CREATE INDEX [IX_FK_AspNetUserRoles_AspNetUsers]
ON [dbo].[AspNetUserRoles]
    ([AspNetUsers_Id]);
GO

-- Creating foreign key on [Cards_CardId] in table 'CardDecks'
ALTER TABLE [dbo].[CardDecks]
ADD CONSTRAINT [FK_CardDecks_Cards]
    FOREIGN KEY ([Cards_CardId])
    REFERENCES [dbo].[Cards]
        ([CardId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Decks_DeckId] in table 'CardDecks'
ALTER TABLE [dbo].[CardDecks]
ADD CONSTRAINT [FK_CardDecks_Decks]
    FOREIGN KEY ([Decks_DeckId])
    REFERENCES [dbo].[Decks]
        ([DeckId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CardDecks_Decks'
CREATE INDEX [IX_FK_CardDecks_Decks]
ON [dbo].[CardDecks]
    ([Decks_DeckId]);
GO

-- Creating foreign key on [Cards_CardId] in table 'CardMechanics'
ALTER TABLE [dbo].[CardMechanics]
ADD CONSTRAINT [FK_CardMechanics_Cards]
    FOREIGN KEY ([Cards_CardId])
    REFERENCES [dbo].[Cards]
        ([CardId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Mechanics_MechanicId] in table 'CardMechanics'
ALTER TABLE [dbo].[CardMechanics]
ADD CONSTRAINT [FK_CardMechanics_Mechanics]
    FOREIGN KEY ([Mechanics_MechanicId])
    REFERENCES [dbo].[Mechanics]
        ([MechanicId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CardMechanics_Mechanics'
CREATE INDEX [IX_FK_CardMechanics_Mechanics]
ON [dbo].[CardMechanics]
    ([Mechanics_MechanicId]);
GO

-- Creating foreign key on [AspNetUsers_Id] in table 'UserCards'
ALTER TABLE [dbo].[UserCards]
ADD CONSTRAINT [FK_UserCards_AspNetUsers]
    FOREIGN KEY ([AspNetUsers_Id])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Cards_CardId] in table 'UserCards'
ALTER TABLE [dbo].[UserCards]
ADD CONSTRAINT [FK_UserCards_Cards]
    FOREIGN KEY ([Cards_CardId])
    REFERENCES [dbo].[Cards]
        ([CardId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserCards_Cards'
CREATE INDEX [IX_FK_UserCards_Cards]
ON [dbo].[UserCards]
    ([Cards_CardId]);
GO

-- Creating foreign key on [Cards1_CardId] in table 'UserDecks'
ALTER TABLE [dbo].[UserDecks]
ADD CONSTRAINT [FK_UserDecks_Cards]
    FOREIGN KEY ([Cards1_CardId])
    REFERENCES [dbo].[Cards]
        ([CardId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Decks1_DeckId] in table 'UserDecks'
ALTER TABLE [dbo].[UserDecks]
ADD CONSTRAINT [FK_UserDecks_Decks]
    FOREIGN KEY ([Decks1_DeckId])
    REFERENCES [dbo].[Decks]
        ([DeckId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserDecks_Decks'
CREATE INDEX [IX_FK_UserDecks_Decks]
ON [dbo].[UserDecks]
    ([Decks1_DeckId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------