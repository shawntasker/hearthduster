﻿Context.tt should contain the namespace to the DataModel Classes found in the Domain Class Library. 
You should have the following in the *.Context.tt file in order to add the namespace.

#>
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using HearthDuster.Domain; <!-- Added -->
<#
if (container.FunctionImports.Any())
{
#>
using System.Data.Entity.Core.Objects;
using System.Linq;
<#
}
#>