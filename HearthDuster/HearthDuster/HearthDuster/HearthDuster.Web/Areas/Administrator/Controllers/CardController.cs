﻿using HearthDuster.DataAccess;
using HearthDuster.Domain;
using HearthDuster.Domain.Enums;
using HearthDuster.Web.Areas.Administrator.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HearthDuster.Web.Helpers;
using System.Net;

namespace HearthDuster.Web.Areas.Administrator.Controllers
{
    [Authorize(Roles ="Administrator")]
    public class CardController : Controller
    {
        hearthdusterdbEntities _db = new hearthdusterdbEntities();

        // GET: Administrator/Card
        [HttpGet]
        public ActionResult Index(string SearchString = "", int page = 1)
        {
            dynamic model = new ExpandoObject();

            const int pageSize = 8;

            var cards = _db.Cards.ToList();

            IEnumerable<Cards> ieCards = cards;

            var pagedCards = new PaginatedList<Cards>(ieCards, page, pageSize);

            model.Cards = pagedCards;

            var images = new List<Files>();

            if(!String.IsNullOrEmpty(SearchString))
            {
                ieCards = ieCards.Where(c => c.Name.Contains(SearchString));
                ieCards.OrderBy(c => c.Name).ThenBy(c => c.Mana);

                foreach(var card in ieCards)
                {
                    if(card.Name.ToLower().Contains(SearchString.ToLower()))
                    {
                        images.Add(_db.Files.Find(card.FileId));
                    }
                }
            }
            else
            {
                ieCards.OrderBy(c => c.Name).ThenBy(c => c.Mana);

                foreach (var card in ieCards)
                {
                    images.Add(_db.Files.Find(card.FileId));
                }
            }

            var pagedImages = new PaginatedList<Files>(images, page, pageSize);

            model.Images = pagedImages;

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            CardCreateViewModel model = new CardCreateViewModel();

            var Mechanics = _db.Mechanics.ToList();

            var MechanicsSelectList = new List<SelectListItem>();

            foreach(var mechanic in Mechanics)
            {
                MechanicsSelectList.Add(new SelectListItem { Value = mechanic.MechanicId.ToString(), Text = mechanic.Name });
            }

            model.Mechanics = MechanicsSelectList;

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CardCreateViewModel model)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    byte[] fileData = new byte[model.FileUpload.InputStream.Length];
                    model.FileUpload.InputStream.Read(fileData, 0, fileData.Length);

                    var newFile = new Domain.Files
                    {
                        UserId = User.Identity.GetUserId(),
                        FileData = fileData,
                        MimeType = model.FileUpload.ContentType,
                        OriginalFileName = model.FileUpload.FileName,
                        DateUploaded = DateTime.Now
                    };

                    var newCard = new Domain.Cards
                    {
                        Text = model.Text,
                        Attack = model.Attack,
                        Name = model.Name,
                        Rarity = Enum.GetName(typeof(Rarity), model.Rarity),
                        Type = Enum.GetName(typeof(CardType), model.Type),
                        CardSet = Enum.GetName(typeof(CardSet), model.Set),
                        Health = model.Health,
                        Mana = model.Mana,
                        FileId = newFile.FileId
                    };

                    _db.Files.Add(newFile);
                    _db.Cards.Add(newCard);
                
                    _db.SaveChanges();

                    TempData["CardCreateSuccess"] = "Card created successfully!";
                    return RedirectToAction("Create");
                }
                catch(Exception ex)
                {
                    TempData["CardCreateError"] = "Sorry there has been an error. Card not saved.";
                    return RedirectToAction("Create");
                }
                
            }
            else
            {
                return View(model);
            }
        }

        public PartialViewResult CardDetails(int id)
        {
            dynamic model = new ExpandoObject();

            var card = _db.Cards.Find(id);

            model.Card = card;
            model.Image = _db.Files.Find(card.FileId);

            return PartialView(model);
        }
    }
}