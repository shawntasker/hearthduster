﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HearthDuster.Domain;
using HearthDuster.DataAccess;
using HearthDuster.Web.Areas.Administrator.ViewModels;
using HearthDuster.Domain.Enums;

namespace HearthDuster.Web.Areas.Administrator.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class DeckController : Controller
    {
        hearthdusterdbEntities _db = new hearthdusterdbEntities();
        // GET: Administrator/Deck
        public ActionResult Index()
        {
            var decks = _db.Decks.ToList();
            var cards = _db.Cards.ToList();

            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            DeckCreateViewModel model = new DeckCreateViewModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(DeckCreateViewModel model) // need to think of how to account for card quantity
        {
            if(ModelState.IsValid)
            {
                var cards = model.Cards;

                var newDeck = new Domain.Decks
                {
                    Class = model.Class.ToString(),
                    Name = model.Name,
                    Cost = model.Cost,
                    Description = model.Description,
                    Author = model.Author
                };

                _db.Decks.Add(newDeck);

                _db.SaveChanges();


                TempData["notice"] = "Deck saved successfully.";
                return RedirectToAction("Index");
            }else
            {
                return View(model);
            }
            
        }



        public ActionResult Details(Decks deck)
        {
            var cards = deck.Cards.ToList();

            return View(cards);
        }
    }
}