﻿using HearthDuster.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HearthDuster.Web.Areas.Administrator.Controllers
{
    public class FileController : Controller
    {
        private hearthdusterdbEntities _db = new hearthdusterdbEntities();
        //
        // GET: Administrator/File
        public ActionResult Index(int id)
        {
            var fileToRetrieve = _db.Files.Find(id);
            return File(fileToRetrieve.FileData, fileToRetrieve.MimeType);
        }
    }
}