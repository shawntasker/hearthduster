﻿using System.Web.Mvc;

namespace HearthDuster.Web.Areas.Administrator
{
    public class AdministratorAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Administrator";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {

            context.MapRoute(
                name: "Card",
                url: "Administrator/{controller}/{action}/{page}",
                defaults: new { controller = "Card", action = "Index", page = UrlParameter.Optional }
           );


            context.MapRoute(
                "Administrator_default",
                "Administrator/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}