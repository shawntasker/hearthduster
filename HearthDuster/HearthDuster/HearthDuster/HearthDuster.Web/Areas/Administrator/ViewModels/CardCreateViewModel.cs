﻿using HearthDuster.Domain;
using HearthDuster.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HearthDuster.Web.Areas.Administrator.ViewModels
{
    public class CardCreateViewModel
    {
        [Required]
        public string Name { get; set; }

        [Range(0, 20, ErrorMessage = "Must be between 0 and 20")]
        public int Health { get; set; }

        [Required]
        [Range(0, 20, ErrorMessage = "Must be between 0 and 20")]
        public int Mana { get; set; }

        [Range(0, 20, ErrorMessage = "Must be between 0 and 20")]
        public int Attack { get; set; }
        public Rarity Rarity { get; set; }
        public CardType Type { get; set; }
        public CardSet Set { get; set; }
        public string Text { get; set; }
        public int? Durability { get; set; }
        public int MechanicId { get; set; }

        [Display(Name = "File Upload")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase FileUpload { get; set; }

        public List<SelectListItem> Mechanics {get; set;}

        public virtual List<Cards> Cards { get; set; }
    }
}