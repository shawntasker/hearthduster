﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using HearthDuster.Domain;
using HearthDuster.Domain.Enums;

namespace HearthDuster.Web.Areas.Administrator.ViewModels
{
    public class DeckCreateViewModel
    {
        [Required]
        public DeckClass Class { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Cost { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string Author { get; set; }

        [Required]
        public virtual List<Cards> Cards { get; set; }

        public List<SelectListItem> DeckClass { get; set; }

    }
}