﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HearthDuster.Web.Startup))]
namespace HearthDuster.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
