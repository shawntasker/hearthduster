﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Migrations;
using System.Collections.Generic;

namespace HearthDuster.Web.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, CheckAndMigrateDatabaseToLatestVersion>());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

    public class CheckAndMigrateDatabaseToLatestVersion : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public CheckAndMigrateDatabaseToLatestVersion():base()
        {
            AutomaticMigrationDataLossAllowed = false;
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            List<IdentityRole> applicationRoles = new List<IdentityRole>()
            {
                new IdentityRole(ApplicationRoles.Administrator),
                new IdentityRole(ApplicationRoles.Standard)
            };


            foreach(var role in applicationRoles)
            {
                if(!RoleManager.RoleExists(role.Name))
                {
                    RoleManager.Create(role);
                }
            }

            base.Seed(context);
        }

        public static class ApplicationRoles
        {
            public static string Administrator = "Administrator";
            public static string Standard = "Standard";
        }

    }
}